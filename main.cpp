#include <MinGE.h>

#include "CameraScript.h"
#include "SerialSync.h"

#define VIDEOMODE sf::VideoMode(960, 768)
#define STYLE sf::Style::Default

void load();

int main()
{
    std::cout << "  -- MinGE --" << std::endl;


    /// Create window
        sf::RenderWindow window(VIDEOMODE, "Gyroscope", STYLE, sf::ContextSettings(24, 0, 0, 4, 3));


    /// Create engine
        Engine* engine = new Engine(&window, 60);
        std::cout << "Seed: " << Random::getSeed() << std::endl;

        load();



    /// Main loop
        engine->start();

        while ( Input::isOpen() )
        {
            /// Handle events
                if (Input::getKeyReleased(sf::Keyboard::F1))
                    AABB::drawAABBs = !AABB::drawAABBs;

                if (Input::getKeyReleased(sf::Keyboard::F2))
                    PhysicEngine::get()->setGravity();

                if (Input::getKeyReleased(sf::Keyboard::F3))
                    GraphicEngine::get()->toggleWireframe();



                if (Input::getKeyReleased(sf::Keyboard::Escape))
                    engine->setPause(true);

                if (Input::getMousePressed(sf::Mouse::Left) && Input::hasFocus())
                    engine->setPause(false);

            /// Render
                if (engine->update())
                    window.display();
        }

        std::cout << '\n' << '\n' << std::endl;

    /// Delete resources
        delete engine;

    #ifdef DEBUG
        sf::sleep(sf::seconds(1.0f));
    #endif // DEBUG

    return 0;
}

void load()
{
    Entity* gyro = Entity::create("gyro")
        ->insert<Graphic>(new Model("Models/bno055/bno055.obj"))
        ->insert<SerialSync>("COM3");

    // Light source
        ModelMaterial* bright = new ModelMaterial("bright");
            bright->ambient = vec3(10.0f/4.0f);
            bright->diffuse = vec3(0.0f);
            bright->specular = vec3(0.0f);
            bright->texture = Texture::get("Textures/white.png");

        Entity::create("Light", false, vec3(2, 2, 2))->insert<Light>(GE_POINT_LIGHT, vec3(0.0f), vec3(1.0f), 0.2f, 1.0f, 0.0f, 0.001f);

    // Camera
        Entity::create("MainCamera", false, vec3(0, 0, 10), vec3(0, 0.499f*PI, PI/2))
            ->insert<Camera>(14, 0.1f, 1000.0f, vec3(0.67f, 0.92f, 1.0f), nullptr, true)
            ->insert<CameraScript>(gyro->find<Transform>());
}
