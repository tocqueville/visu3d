/*
* Author: Manash Kumar Mandal
* Modified Library introduced in Arduino Playground which does not work
* This works perfectly
* LICENSE: MIT
*/

#include "SerialPort.h"

SerialPort::SerialPort(const char *portName, DWORD baudRate)
{
    connected = false;

    handler = CreateFileA(static_cast<LPCSTR>(portName),
                                GENERIC_READ | GENERIC_WRITE,
                                0,
                                NULL,
                                OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL,
                                NULL);

    if (handler == INVALID_HANDLE_VALUE)
    {
        if (GetLastError() == ERROR_FILE_NOT_FOUND)
            printf("ERROR: Handle was not attached. Reason: %s not available\n", portName);
        else
            printf("Unknown error");
    }
    else
    {
        DCB dcbSerialParameters = {};

        if (GetCommState(handler, &dcbSerialParameters))
        {
            dcbSerialParameters.BaudRate = baudRate;
            dcbSerialParameters.ByteSize = 8;
            dcbSerialParameters.StopBits = ONESTOPBIT;
            dcbSerialParameters.Parity = NOPARITY;
            dcbSerialParameters.fDtrControl = DTR_CONTROL_ENABLE;

            connected = SetCommState(handler, &dcbSerialParameters);
            if (connected)
                PurgeComm(handler, PURGE_RXCLEAR | PURGE_TXCLEAR);
            else
                printf("ALERT: could not set Serial port parameters\n");
        }
        else
            printf("failed to get current serial parameters");
    }
}

SerialPort::~SerialPort()
{
    if (connected){
        connected = false;
        CloseHandle(handler);
    }
}

int SerialPort::readSerialPort(char *buffer, unsigned int buf_size)
{
    DWORD bytesRead;
    unsigned int toRead = 0;

    ClearCommError(handler, &errors, &status);

    if (status.cbInQue > 0)
    {
        if (status.cbInQue > buf_size)
            toRead = buf_size;
        else
            toRead = status.cbInQue;
    }

    if (ReadFile(handler, buffer, toRead, &bytesRead, NULL))
        return bytesRead;
    return 0;
}

bool SerialPort::writeSerialPort(char *buffer, unsigned int buf_size)
{
    DWORD bytesSend;

    if (!WriteFile(handler, (void*) buffer, buf_size, &bytesSend, 0))
    {
        ClearCommError(handler, &errors, &status);
        return false;
    }
    else
        return true;
}

bool SerialPort::isConnected()
{
    return connected;
}
