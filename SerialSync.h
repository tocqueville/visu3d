#include <iostream>
#include <string>
#include <sstream>

#include "SerialPort.h"

#define MAX_DATA_LENGTH 255

class SerialSync : public Script
{
    public:
        SerialSync(std::string _port):
            port(_port), arduino(nullptr)
        { }

        void start()
        {
            arduino = new SerialPort(port.c_str(), CBR_115200);

            if (arduino->isConnected())
                std::cout << "Connection established at port " << port << std::endl;

            else
                std::cout << "Unable to find arduino on port " << port << std::endl;

            lines.push_back(std::string());
        }

        void onDestroy()
        {
            delete arduino;
        }

        void update()
        {
            read();
            extract();
        }

        void read()
        {
            if (arduino->isConnected())
            {
                char data[MAX_DATA_LENGTH];
                int numBytes = arduino->readSerialPort(data, MAX_DATA_LENGTH);

                for (int i(0); i < numBytes; ++i)
                {
                    if (data[i] == '\n')
                        lines.push_back(std::string());
                    else
                        lines.back().push_back(data[i]);
                }
            }
        }

        void extract()
        {
            if (lines.size() > 1)
            {
                std::istringstream line(lines.front());

                char type; line >> type;

                switch (type)
                {
                    case 'r':
                        vec3 rotation;
                            line >> rotation.x;
                            line >> rotation.y;
                            line >> rotation.z;

                        tr->setRotation(radians(rotation));
                        break;
                }

                lines.pop_front();
            }
        }

    private:
        std::string port;
        SerialPort *arduino;

        std::list<std::string> lines;
};
