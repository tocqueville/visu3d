/*
* Author: Manash Kumar Mandal
* Modified Library introduced in Arduino Playground which does not work
* This works perfectly
* LICENSE: MIT
*/


#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

class SerialPort
{
    public:
        SerialPort(const char *portName, DWORD baudRate);
        ~SerialPort();

        int readSerialPort(char *buffer, unsigned int buf_size);
        bool writeSerialPort(char *buffer, unsigned int buf_size);
        bool isConnected();

    private:
        HANDLE handler;
        bool connected;
        COMSTAT status;
        DWORD errors;
};

#endif // SERIALPORT_H
